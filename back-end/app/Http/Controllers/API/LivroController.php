<?php

namespace App\Http\Controllers\API;

use App\Livro;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;

class LivroController extends BaseController
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'titulo' => 'required|string|max:255',
            'descricao' => 'required|string|max:500',
        ]);
        if ($validator->fails())
        {
           return $this->enviarRespostaErro('Erro de validação.', $validator->errors());
        }

        $livro = Livro::create($request->all());
        return $this->enviarRespostaSucesso($livro, 'Livro criado com sucesso.', 201);
    }

    public function update (Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id' => 'required|integer',
            'titulo' => 'required|string|max:255',
            'descricao' => 'required|string|max:500',
        ]);

        if ($validator->fails())
        {
           return $this->enviarRespostaErro('Erro de validação.', $validator->errors());
        }

        $livro = Livro::find($request->id);
        if(!$livro)
        {
            return $this->enviarRespostaErro('O livro não existe.', null, 400);
        }
        $livro->update($request->all());
        return $this->enviarRespostaSucesso($livro, 'Livro atualizado com sucesso.', 200);
    }

    public function destroy(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id' => 'required|integer',
        ]);

        if($validator->fails())
        {
            return $this->enviarRespostaErro('Erro de Validação.', $validator->errors());
        }

        $livro = Livro::find($request->id);
        if(!$livro)
        {
            return $this->enviarRespostaErro('O livro não existe.', $validator->errors());
        }

        $livro->delete();
        return $this->enviarRespostaSucesso(null, 'Livro deletado com sucesso.', 204);
    }

    public function show(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required|integer',
        ]);

        if($validator->fails())
        {
            return $this->enviarRespostaErro('Erro de validação.', $validator->errors());
        }

        $livro = Livro::find($request->id);
        if(!$livro)
        {
            return $this->enviarRespostaErro('O livro não existe.', $validator->errors());
        }
        return $this->enviarRespostaSucesso($livro, 'Livro mostrado com sucesso.', 200);
    }

    public function index(Request $request)
    {
        $livro = Livro::all();
        if($livro->count() > 0)
        {
            return $this->EnviarRespostaSucesso($livro, 'Livros motrados com sucesso.');
        }
        return $this->EnviarRespostaErro('Não há usuários.');
    }
}
