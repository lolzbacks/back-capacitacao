<?php

namespace App\Http\Controllers\API;
use App\Livro;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;

class FavoritoController extends BaseController
{
    public function favoritar(Request $request)
    {   
        $validator = Validator::make($request->all(),[
            'user_id' => 'required | integer',
            'livro_id' => 'required|integer',
        ]);

        if($validator->fails())
        {
            return $this->enviarRespostaErro('Erro de validação.', $validator->errors());
        }

        $favorito = Livro::find($request->livro_id);
        if(!$favorito)
        {
            return $this->enviarRespostaErro('O livro não existe.', $validator->errors());
        }

        $usuario = User::find($request->user_id);
        if(!$usuario)
        {
            return $this->enviarRespostaErro('o usuário não existe', $validator->errors());
        }

        if($usuario->livros->contains($request->livro_id))
        {
            return $this->enviarRespostaErro('o usuário já favoritou esse livro', $validator->errors());
        }
        $usuario->livros()->attach($favorito);
        return $this->enviarRespostaSucesso($favorito, 'livro favoritado com sucesso', 200);
    }

    public function desfavoritar(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'user_id' => 'required | integer',
            'livro_id' => 'required|integer',
        ]);

        if($validator->fails())
        {
            return $this->enviarRespostaErro('Erro de validação.', $validator->errors());
        }

        $favorito = Livro::find($request->livro_id);
        if(!$favorito)
        {
            return $this->enviarRespostaErro('O livro não existe.', $validator->errors());
        }

        $usuario = User::find($request->user_id);
        if(!$usuario)
        {
            return $this->enviarRespostaErro('o usuário não existe', $validator->errors());
        }

        if(!$usuario->livros->contains($request->livro_id))
        {
            return $this->enviarRespostaErro('o usuário não favoritou esse livro', $validator->errors());
        }
        $usuario->livros()->detach($favorito);
        return $this->enviarRespostaSucesso($favorito, 'livro desfavoritado com sucesso', 200);
    }

    public function show(Request $request)
    {   
        $validator = Validator::make($request->all(),[
            'user_id' => 'required | integer',
        ]);

        if($validator->fails())
        {
            return $this->enviarRespostaErro('Erro de validação.', $validator->errors());
        }

        $usuario = User::find($request->user_id);
        if(!$usuario)
        {
            return $this->enviarRespostaErro('o usuário não existe.', $validator->errors());
        }
        
        if($usuario->livros->count() > 0)
        {
            return $this->enviarRespostaSucesso($usuario->livros, 'livros favoritados.', 200);
        }

        return $this->enviarRespostaErro($usuario->livros, 'O usuário não favoritou nenhum livro.', 200);
    }
}    