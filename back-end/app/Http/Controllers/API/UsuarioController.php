<?php

namespace App\Http\Controllers\API;

use Illuminate\Validation\Rule;
use Validator;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use Illuminate\Http\Request;

class UsuarioController extends BaseController
{

    public function update (Request $request)
    {
        $validator = Validator::make($request->all(),[
            'id' => 'required|integer',
            'endereco' => 'required|string|max:255',
            'numero' => 'required|string|max:255',
            'cargo' => 'required|string|max:255',
            'nome' => 'required|string|max:255',
            'email' =>  'required',
            Rule::unique('users')->ignore($request->id),
        ]);
        if ($validator->fails())
        {
            return $this->enviarRespostaErro('Erro de validação.', $validator->errors());
        }

        $usuario = User::find($request->id);
        if(!$usuario)
        {
            return $this->enviarRespostaErro('O usuário não existe.', null, 400);
        }

        $usuario->email = $request->email;
        $usuario->endereco = $request->endereco;
        $usuario->numero = $request->numero;
        $usuario->cargo = $request->cargo;
        $usuario->nome = $request->nome;

        $usuario->save();

        return $this->enviarRespostaSucesso($usuario, 'Dados alterados com sucesso.', 201);
    }

    public function show(Request $request)
    {   

        $validator = Validator::make($request->all(),[
            'id' => 'required|integer',
        ]);
        if($validator->fails())
        {
            return $this->enviarRespostaErro('Erro de validação.', $validator->errors());
        }

        $usuario = User::find($request->id);
        if(!$usuario)
        {
            return $this->enviarRespostaErro('O usuário não existe.', null, 400);
        }
        return $this->enviarRespostaSucesso($usuario, 'Dados de usuário.', 201);
    }

    public function index(Request $request)
    {
        $usuario = User::all();
        if($usuario->count() > 0)
        {
            return $this->enviarRespostaSucesso($usuario,'Usuários mostrados com sucesso');
        }
        return $this->enviarRespostaErro('Não há usuários.');
    }
}    

