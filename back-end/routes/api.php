<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'API\AuthController@login');
Route::post('registro', 'API\AuthController@registro');

Route::middleware('auth:api')->group(function(){
    //rotas user
    Route::post('dadosUsuario', 'API\UsuarioController@store');
    Route::post('atualizaUsuario', 'API\UsuarioController@update');
    Route::post('mostraUsuario', 'API\UsuarioController@show');
    Route::get('mostraUsuarios', 'API\UsuarioController@index');
    //livro
    Route::post('criaLivro', 'API\LivroController@store');
    Route::post('atualizaLivro', 'API\LivroController@update');
    Route::post('deletaLivro', 'API\LivroController@destroy');
    Route::post('mostraLivro', 'API\LivroController@show');
    Route::get('mostraLivros', 'API\LivroController@index');

    Route::post('favoritaLivros', 'API\FavoritoController@favoritar');
    Route::post('desfavoritaLivros', 'API\FavoritoController@desfavoritar');
    Route::post('mostraFavoritos', 'API\FavoritoController@show');
    
    Route::middleware('admin')->group(function(){
        // rotas admin
        //tem nada
    });    
    //logout
    Route::get('logout', 'API\AuthController@logout');
});

